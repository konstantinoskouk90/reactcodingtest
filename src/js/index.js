import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import '../assets/stylus/app.styl';
import App from './app/app';

ReactDOM.render(
  <App />,
  document.getElementById('root')
);